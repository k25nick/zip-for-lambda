# zip-for-lambda

Easily zip files for use as AWS Lamda functions.

## Description

This project is intended for use from npm scripts to simplify the process of zipping up Javascript Lambda Functions ready for deployment to AWS.

## Pre-requisites

This script relies on having the "zip" shell command installed.

## Usage

It is assumed that the Javascript files to zip are in a dist folder relative to the package.json file of the project or mono-repo sub-project.

The script takes one parameter, the base name to use for the zip file, so:

`
"scripts": {
      "zip": "zip-for-lambda getUser"
  }
`

Will take all ".js" files from the dist folder and produce a "getUser.zip" file in the same dist folder.

## License

MIT
