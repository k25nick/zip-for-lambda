const execSync = require('child_process').execSync;

const args = process.argv;
if (args.length === 3) {
    execSync(`zip ${args[2]}.zip *.js`, { cwd: './dist'});
}
