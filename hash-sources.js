/**
 * Scan source files in a project and produce a hash of the file contents in the project dist
 * folder to assist with determining if a project has changed from one build to the next.
 */
const { resolve } = require('path');
const { readdir } = require('fs').promises;
 
async function* getFiles(dir) {
  const dirents = await readdir(dir, { withFileTypes: true });
  for (const dirent of dirents) {
    const res = resolve(dir, dirent.name);
    if (dirent.isDirectory()) {
      yield* getFiles(res);
    } else {
      yield res;
    }
  }
}

;(async () => {
    for await (const f of getFiles('.')) {
      console.log(f);
    }
  })()
